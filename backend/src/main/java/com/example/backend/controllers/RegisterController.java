package com.example.backend.controllers;

import com.example.backend.auth.AuthenticationResponse;
import com.example.backend.config.JwtService;
import com.example.backend.domain.dto.RegisterForm;
import com.example.backend.domain.mapper.UserMapper;
import com.example.backend.services.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class RegisterController {

    private final RegisterService registerService;
    private final UserMapper userMapper;
    private final JwtService jwtService;

    @PostMapping(value = "/register")
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin(origins = "http://localhost:3000/")
    public AuthenticationResponse register(@RequestBody RegisterForm registerForm) {
        registerService.register(registerForm);
        var jwtToken = jwtService.generateToken(userMapper.toUser(registerForm));
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
