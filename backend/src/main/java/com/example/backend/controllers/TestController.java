package com.example.backend.controllers;

import com.example.backend.domain.dto.TestDto;
import com.example.backend.domain.entity.Test;
import com.example.backend.services.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:3000/")
public class TestController {
    private final TestService testService;

    @GetMapping("/test")
    @ResponseStatus(HttpStatus.OK)
    TestDto getTest(@RequestParam("testName") String testName) {
        return testService.getTestByTestName(testName);
    }

    @PostMapping("/test")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTest(@RequestBody Test test) {
        testService.createTest(test);
    }

    //    @GetMapping("/test/{testId}")
//    @ResponseStatus(HttpStatus.OK)
//    @CrossOrigin
//    TestDto getTest(@PathVariable Long testId) {
//        return testService.getTest(testId);
//    }
}
