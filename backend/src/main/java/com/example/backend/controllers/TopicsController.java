package com.example.backend.controllers;

import com.example.backend.domain.dto.TopicDto;
import com.example.backend.domain.entity.Course;
import com.example.backend.services.TopicsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:3000")
public class TopicsController {

    private final TopicsService topicsService;

    @GetMapping("/courses/english/topics")
    @ResponseStatus(HttpStatus.OK)
    TopicDto findTopicByTopicName(@RequestParam("topicId") Long topicId) {
        System.out.println(topicId);
        return topicsService.findByTopicId(topicId);
    }
    @GetMapping("/courses/{course}/topics")
    @ResponseStatus(HttpStatus.OK)
    List<TopicDto> findAllTopicsByCourse(@PathVariable("course") Course course) {
        return topicsService.findAllTopicsByCourse(course);
    }
}
