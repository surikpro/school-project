package com.example.backend.controllers;

import com.example.backend.domain.entity.TestResult;
import com.example.backend.services.TestResultsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class TestResultsController {

    private final TestResultsService testResultsService;

    @PostMapping("/testResults")
    @CrossOrigin(origins = "http://localhost:3000/")
    public void createTestResult(@RequestBody TestResult testResult) {
        testResultsService.save(testResult);
    }
}
