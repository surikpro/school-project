package com.example.backend.controllers;

import com.example.backend.domain.dto.CourseDto;
import com.example.backend.services.CoursesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:3000/")
public class CoursesController {
    private final CoursesService coursesService;

    @GetMapping("/courses/findAll")
    @ResponseStatus(HttpStatus.OK)
    List<CourseDto> findAllCourses() {
        return coursesService.findAllCourses();
    }
}
