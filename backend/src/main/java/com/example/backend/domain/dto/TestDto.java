package com.example.backend.domain.dto;

import com.example.backend.domain.entity.User;
import com.example.backend.domain.entity.Question;
import com.example.backend.domain.entity.TestResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestDto {

    private Long testId;

    private String testName;

    private List<Question> questions;

    private List<TestResult> testResults;

    private User user;
}
