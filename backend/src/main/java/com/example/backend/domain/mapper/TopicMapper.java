package com.example.backend.domain.mapper;

import com.example.backend.domain.dto.TopicDto;
import com.example.backend.domain.entity.Topic;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TopicMapper {
    TopicDto toTopicDto(Topic topic);
}
