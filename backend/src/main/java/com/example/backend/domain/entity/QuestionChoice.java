package com.example.backend.domain.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class QuestionChoice implements Serializable {

    @Serial
    private static final long serialVersionUID = -5649997961739050891L;
    @Id
    @Column(name = "question_choice_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long questionChoiceId;

    @Column(name = "question_choice")
    private String questionChoice;

}
