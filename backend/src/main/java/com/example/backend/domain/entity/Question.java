package com.example.backend.domain.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Question implements Serializable {

    @Serial
    private static final long serialVersionUID = 5332740171013770887L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long questionId;

    @Column(name = "question")
    private String question;

    @Column(name = "answer")
    private String answer;

    @OneToMany(cascade = CascadeType.ALL)
    private List<QuestionChoice> questionChoices;

}
