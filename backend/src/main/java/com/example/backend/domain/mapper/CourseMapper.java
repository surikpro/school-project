package com.example.backend.domain.mapper;

import com.example.backend.domain.dto.CourseDto;
import com.example.backend.domain.entity.Course;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseMapper {
    CourseDto toCourseDto(Course course);
}
