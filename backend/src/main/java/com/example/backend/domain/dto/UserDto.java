package com.example.backend.domain.dto;

import com.example.backend.domain.entity.Test;
import com.example.backend.domain.entity.TestResult;
import com.example.backend.domain.entity.User;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    private Long userId;
    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private String email;
    private User.Role role;
    private User.State state;
    private List<TestResult> testResults;
    private List<Test> tests;
}
