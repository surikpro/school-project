package com.example.backend.domain.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Setter
@Getter
@Builder
public class RegisterForm {
    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private String email;
    private String password;
}
