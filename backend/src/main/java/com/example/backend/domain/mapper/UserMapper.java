package com.example.backend.domain.mapper;

import com.example.backend.domain.dto.RegisterForm;
import com.example.backend.domain.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toUser(RegisterForm registerForm);
}
