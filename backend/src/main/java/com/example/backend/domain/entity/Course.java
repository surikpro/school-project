package com.example.backend.domain.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Course implements Serializable {

    @Serial
    private static final long serialVersionUID = -6089600470123211351L;

    @Id
    @Column(name = "course_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long courseId;

    @Column(name = "course_name")
    private String courseName;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<Topic> topicsList;
}
