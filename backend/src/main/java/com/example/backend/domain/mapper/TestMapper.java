package com.example.backend.domain.mapper;

import com.example.backend.domain.dto.TestDto;
import com.example.backend.domain.entity.Test;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TestMapper {
    TestDto toTestDto(Test test);
}
