package com.example.backend.domain.dto;

import com.example.backend.domain.entity.Topic;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseDto {

    private Long courseId;

    private String courseName;

    private List<Topic> topicsList;
}
