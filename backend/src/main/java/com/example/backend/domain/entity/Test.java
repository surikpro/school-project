package com.example.backend.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Test implements Serializable {

    @Serial
    private static final long serialVersionUID = 6997824552918369114L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "test_id")
    private Long testId;

    @Column(name = "test_name")
    private String testName;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Question> questions;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy ="test", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<TestResult> testResults;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(value = EnumType.STRING)
    private State state;

    public enum State {
        NOT_COMPLETED, COMPLETED
    }
}
