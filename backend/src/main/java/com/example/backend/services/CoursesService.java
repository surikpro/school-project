package com.example.backend.services;

import com.example.backend.domain.dto.CourseDto;

import java.util.List;

public interface CoursesService {
    List<CourseDto> findAllCourses();
}
