package com.example.backend.services;

import com.example.backend.domain.dto.TestDto;
import com.example.backend.domain.entity.Test;

public interface TestService {
    TestDto getTest(Long testId);

    TestDto getTestByTestName(String testName);

    void createTest(Test test);
}
