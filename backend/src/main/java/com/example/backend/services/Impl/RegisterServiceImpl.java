package com.example.backend.services.Impl;

import com.example.backend.domain.dto.RegisterForm;
import com.example.backend.domain.entity.User;
import com.example.backend.exceptions.UserFoundException;
import com.example.backend.repositories.UsersRepository;
import com.example.backend.services.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RegisterServiceImpl implements RegisterService {
    private final PasswordEncoder passwordEncoder;
    private final UsersRepository usersRepository;
    @Override
    public void register(RegisterForm registerForm) {
        User user = User.builder()
                .firstName(registerForm.getFirstName())
                .lastName(registerForm.getLastName())
                .birthday(registerForm.getBirthday())
                .email(registerForm.getEmail())
                .password(passwordEncoder.encode(registerForm.getPassword()))
                .createdAt(new Date())
                .role(User.Role.USER)
                .state(User.State.NOT_CONFIRMED)
                .confirmUUID(UUID.randomUUID().toString())
                .build();
        if (usersRepository.findByEmail(registerForm.getEmail()).isEmpty()) {
            usersRepository.save(user);
        } else {
            throw new UserFoundException(registerForm.getEmail());
        }

    }
}
