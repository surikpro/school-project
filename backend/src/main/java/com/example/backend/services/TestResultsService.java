package com.example.backend.services;

import com.example.backend.domain.entity.TestResult;

public interface TestResultsService {
    void save(TestResult testResult);
}
