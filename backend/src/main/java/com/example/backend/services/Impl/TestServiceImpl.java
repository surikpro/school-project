package com.example.backend.services.Impl;

import com.example.backend.domain.dto.TestDto;
import com.example.backend.domain.entity.Test;
import com.example.backend.domain.mapper.TestMapper;
import com.example.backend.exceptions.TestNotFoundException;
import com.example.backend.repositories.TestRepository;
import com.example.backend.services.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestServiceImpl implements TestService {

    private final TestRepository testRepository;
    private final TestMapper testMapper;

    @Override
    public TestDto getTest(Long testId) {
        return Optional.of(testId)
                .map(testRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(testMapper::toTestDto)
                .orElseThrow(() -> new TestNotFoundException(testId));
    }

    @Override
    public TestDto getTestByTestName(String testName) {
        return Optional.of(testName)
                .map(testRepository::findTestByTestName)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(testMapper::toTestDto)
                .orElseThrow(() -> new TestNotFoundException(testName));
    }

    @Override
    public void createTest(Test test) {
        testRepository.save(test);
    }
}
