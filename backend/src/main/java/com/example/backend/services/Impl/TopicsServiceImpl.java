package com.example.backend.services.Impl;

import com.example.backend.domain.dto.TopicDto;
import com.example.backend.domain.entity.Course;
import com.example.backend.domain.entity.Topic;
import com.example.backend.domain.mapper.TopicMapper;
import com.example.backend.exceptions.TestNotFoundException;
import com.example.backend.exceptions.TopicNotFoundException;
import com.example.backend.repositories.TopicsRepository;
import com.example.backend.services.TopicsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TopicsServiceImpl implements TopicsService {
    private final TopicsRepository topicsRepository;
    private final TopicMapper topicMapper;
    @Override
    public TopicDto findTopicByTopicName(String name) {
        return Optional.of(name)
                .map(topicsRepository::findByTopicName)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(topicMapper::toTopicDto)
                .orElseThrow(() -> new TopicNotFoundException(name));
    }

    @Override
    public List<Topic> findAllTopics() {
        return topicsRepository.findAll();
    }

    @Override
    public TopicDto findByTopicId(Long topicId) {
        return Optional.of(topicId)
                .map(topicsRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(topicMapper::toTopicDto)
                .orElseThrow(() -> new TopicNotFoundException(topicId));
    }

    @Override
    public List<TopicDto> findAllTopicsByCourse(Course course) {
        return topicsRepository
                .findAllTopicsByCourse(course)
                .stream()
                .map(topicMapper::toTopicDto)
                .collect(Collectors.toList());
    }
}
