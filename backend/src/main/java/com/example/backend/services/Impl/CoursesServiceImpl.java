package com.example.backend.services.Impl;

import com.example.backend.domain.dto.CourseDto;
import com.example.backend.domain.entity.Course;
import com.example.backend.domain.mapper.CourseMapper;
import com.example.backend.repositories.CoursesRepository;
import com.example.backend.services.CoursesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CoursesServiceImpl implements CoursesService {
    private final CoursesRepository coursesRepository;
    private final CourseMapper courseMapper;
    @Override
    public List<CourseDto> findAllCourses() {
        return coursesRepository
                .findAll()
                .stream()
                .map(courseMapper::toCourseDto)
                .collect(Collectors.toList());
    }
}
