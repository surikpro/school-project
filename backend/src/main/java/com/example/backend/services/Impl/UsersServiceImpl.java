package com.example.backend.services.Impl;

import com.example.backend.repositories.UsersRepository;
import com.example.backend.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;
}
