package com.example.backend.services.Impl;

import com.example.backend.domain.entity.TestResult;
import com.example.backend.repositories.TestResultsRepository;
import com.example.backend.services.TestResultsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestResultsServiceImpl implements TestResultsService {

    private final TestResultsRepository testResultsRepository;

    @Override
    public void save(TestResult testResult) {
        testResultsRepository.save(testResult);
    }
}
