package com.example.backend.services;

import com.example.backend.domain.dto.RegisterForm;

public interface RegisterService {
    void register(RegisterForm registerForm);
}
