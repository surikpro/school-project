package com.example.backend.services;

import com.example.backend.domain.dto.TopicDto;
import com.example.backend.domain.entity.Course;
import com.example.backend.domain.entity.Topic;

import java.util.List;

public interface TopicsService {
    TopicDto findTopicByTopicName(String name);
    List<Topic> findAllTopics();

    TopicDto findByTopicId(Long topicId);

    List<TopicDto> findAllTopicsByCourse(Course course);
}
