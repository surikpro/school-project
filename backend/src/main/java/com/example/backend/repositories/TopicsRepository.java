package com.example.backend.repositories;

import com.example.backend.domain.entity.Course;
import com.example.backend.domain.entity.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TopicsRepository extends JpaRepository<Topic, Long> {
    Optional<Topic> findByTopicName(String topicName);
    List<Topic> findAllTopicsByCourse(Course course);

}
