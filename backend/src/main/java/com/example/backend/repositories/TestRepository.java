package com.example.backend.repositories;

import com.example.backend.domain.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {
    @Override
    Optional<Test> findById(Long testId);

    Optional<Test> findTestByTestName(String testName);
}
