package com.example.backend.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.FOUND;

@ResponseStatus(value = FOUND)
public class UserFoundException extends RuntimeException {
    public UserFoundException(String email) {
        super("User with email - " + email + " is already exists");
    }
}
