package com.example.backend.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(value = NOT_FOUND)
public class TestNotFoundException extends RuntimeException {
    public TestNotFoundException(Long testId) {
        super("Test with id - " + testId + " is not found");
    }
    public TestNotFoundException(String testName) {
        super("Test with name - " + testName + " is not found");
    }
}
