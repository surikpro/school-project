package com.example.backend.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(value = NOT_FOUND)
public class TopicNotFoundException extends RuntimeException {
    public TopicNotFoundException(String name) {
        super("Topic with name - " + name + " is not found");
    }
    public TopicNotFoundException(Long topicId) {
        super("Topic with id - " + topicId + " is not found");
    }
}
