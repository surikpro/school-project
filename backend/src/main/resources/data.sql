{
    "testLevel": "easy",
    "questions": [
        {
            "question": "Hi! What's ......?",
            "answer": "your name",
            "questionChoices": [
                {
                    "questionChoice": "you name"
                },
                {
                    "questionChoice": "your name"
                },
                {
                    "questionChoice": "the name"
                },
                {
                    "questionChoice": "name"
                }
            ]},
    {
            "question": "Mr Green is ...... English teacher.",
            "answer": "our",
            "questionChoices": [
                {
                    "questionChoice": "our"
                },
                {
                    "questionChoice": "us"
                },
                {
                    "questionChoice": "we"
                },
                {
                    "questionChoice": "you"
                }
            ]},
    {
            "question": "How ...... you today?",
            "answer": "are",
            "questionChoices": [
                        {
                            "questionChoice": "are"
                        },
                        {
                            "questionChoice": "is"
                        },
                        {
                            "questionChoice": "be"
                        },
                        {
                            "questionChoice": "am"
                        }
                    ]},
    {
            "question": "We are ...... the classroom.",
            "answer": "in",
            "questionChoices": [
                {
                    "questionChoice": "on"
                },
                {
                    "questionChoice": "in"
                },
                {
                    "questionChoice": "at"
                },
                {
                    "questionChoice": "with"
                }
            ]},
    {
            "question": "...... are fifteen students in my class.",
            "answer": "There",
            "questionChoices": [
                {
                    "questionChoice": "These"
                },
                {
                    "questionChoice": "Them"
                },
                {
                    "questionChoice": "There"
                },
                {
                    "questionChoice": "Their"
                }
            ]},
    {
            "question": "Look at ...... aeroplane in the sky! It's very big!",
            "answer": "that",
            "questionChoices": [
                {
                    "questionChoice": "these"
                },
                {
                    "questionChoice": "this"
                },
                {
                    "questionChoice": "it"
                },
                {
                    "questionChoice": "that"
                }
            ]},
    {
            "question": "...... the time? - It's five o'clock.",
            "answer": "What's",
            "questionChoices": [
                {
                    "questionChoice": "What's"
                },
                {
                    "questionChoice": "Where's"
                },
                {
                    "questionChoice": "When's"
                },
                {
                    "questionChoice": "How's"
                }
            ]},
    {
            "question": "Franco comes ...... Costa Rica.",
            "answer": "from",
            "questionChoices": [
                {
                    "questionChoice": "for"
                },
                {
                    "questionChoice": "in"
                },
                {
                    "questionChoice": "at"
                },
                {
                    "questionChoice": "from"
                }
            ]},
    {
             "question": "Where do you ...... from? - Barcelona in Spain.",
            "answer": "come",
            "questionChoices": [
                {
                    "questionChoice": "come"
                },
                {
                    "questionChoice": "comes"
                },
                {
                    "questionChoice": "be"
                },
                {
                    "questionChoice": "go"
                }
            ]},
    {
            "question": "Franco ...... like eating English breakfast.",
            "answer": "doesn't",
            "questionChoices": [
                {
                    "questionChoice": "don't"
                },
                {
                    "questionChoice": "doesn't"
                },
                {
                    "questionChoice": "aren't"
                },
                {
                    "questionChoice": "isn't"
                }
            ]}
    ]
}