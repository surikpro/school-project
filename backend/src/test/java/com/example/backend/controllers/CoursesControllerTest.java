package com.example.backend.controllers;

import com.example.backend.domain.dto.CourseDto;
import com.example.backend.services.CoursesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Arrays;
import java.util.List;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CoursesControllerTest {

    @Mock
    private CoursesService coursesService;
    @InjectMocks
    private CoursesController coursesController;

    @Test
    public void coursesController_testFindAllCourses_ReturnCourseDtos() {
        CourseDto course1 = CourseDto.builder()
                .courseName("English")
                .build();
        CourseDto course2 = CourseDto.builder()
                .courseName("Math")
                .build();
        List<CourseDto> expectedCourses = Arrays.asList(course1, course2);
        when(coursesService.findAllCourses()).thenReturn(expectedCourses);
        List<CourseDto> actualCourses = coursesController.findAllCourses();
        Assertions.assertEquals(expectedCourses, actualCourses);
    }
}
