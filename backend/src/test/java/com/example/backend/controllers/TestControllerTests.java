package com.example.backend.controllers;

import com.example.backend.domain.dto.TestDto;
import com.example.backend.domain.entity.Test;
import com.example.backend.services.TestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
public class TestControllerTests {
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Mock
    private TestService testService;
    @InjectMocks
    private TestController testController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(testController).build();
        objectMapper = new ObjectMapper();
    }
    @org.junit.jupiter.api.Test
    void testController_GetTest_ReturnTestByName() throws Exception {
        String testName = "english";
        TestDto testDto = new TestDto();
        testDto.setTestName(testName);

        when(testService.getTestByTestName(testName)).thenReturn(testDto);

        mockMvc.perform(get("/api/v1/test")
                .param("testName", testName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.testName").value(testName));
        verify(testService, times(1)).getTestByTestName(testName);
    }
    @org.junit.jupiter.api.Test
    void testController_CreateTest_ReturnCreate() throws Exception {
        Test test = new Test();
        test.setTestName("english");

        mockMvc.perform(post("/api/v1/test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(test)))
                .andExpect(status().isCreated());
        verify(testService, times(1)).createTest(test);
    }
}
