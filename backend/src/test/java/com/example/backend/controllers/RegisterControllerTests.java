package com.example.backend.controllers;

import com.example.backend.config.JwtService;
import com.example.backend.domain.dto.RegisterForm;
import com.example.backend.domain.entity.User;
import com.example.backend.domain.mapper.UserMapper;
import com.example.backend.services.RegisterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.time.LocalDate;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class RegisterControllerTests {
    @Mock
    private RegisterService registerService;
    @Mock
    private UserMapper userMapper;
    @Mock
    JwtService jwtService;

    @InjectMocks
    private RegisterController registerController;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(registerController).build();
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void registerController_CreateUser_ReturnCreatedWithJwt() throws Exception {
        RegisterForm registerForm = RegisterForm.builder()
                .firstName("aydar")
                .lastName("zaka")
                .birthday(LocalDate.of(2000, 6, 15))
                .email("micky@mail.com")
                .password("1234")
                .build();

        String jwtToken = "mockJwtToken";
        doNothing().when(registerService).register(any(RegisterForm.class));
        when(userMapper.toUser(any(RegisterForm.class))).thenReturn(new User());
        when(jwtService.generateToken(any(User.class))).thenReturn(jwtToken);

        mockMvc.perform(post("/api/v1/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerForm)))
                .andExpect(status().isCreated());


    }
}
