import React from 'react';
import { Sidebar } from '../Sidebar/Sidebar';
import { Navbar } from '../Navbar/Navbar';
import { Footer } from '../Footer/Footer';
import aboutUs from '../../assets/images/aboutus.jpeg';

export const About = () => {
  return (
    <div className='bg-gray-100'>
        <Sidebar />
        <Navbar />
        <div className="bg-gray-300 mt-16 mb-10 rounded-md mx-auto w-3/5">
          <div className="my-6 w-full p-1">
            <img
              alt="nature"
              className="border-gray-900 rounded-md h-[32rem] w-full object-cover object-center"
              src={aboutUs}
            />
          </div>
          <h1 className="text-gray-900 mb-1 text-2xl font-semibold p-3">
            What is KIDS ACADEMY
          </h1>
          <p className="text-gray-900 font-normal p-3">
          One of the leading online schools with innovative approach to learning language.
          The best learning practices with flexible facilitation and online class management.
          We offer the communicative approach to learning and highly skilled with many years of experience.
          Can you help me out? you will get a lot of free exposure doing this
          can my website be in english?. There is too much white space do less
          with more, so that will be a conversation piece can you rework to make
          the pizza look more delicious other agencies charge much lesser can
          you make the blue bluer?. I think we need to start from scratch can my
          website be in english?, yet make it sexy i&apos;ll pay you in a week
          we don&apos;t need to pay upfront i hope you understand can you make
          it stand out more?. Make the font bigger can you help me out? you will
          get a lot of free exposure doing this that&apos;s going to be a chunk
          of change other agencies charge much lesser. Are you busy this
          weekend? I have a new project with a tight deadline that&apos;s going
          to be a chunk of change. There are more projects lined up charge extra
          the next time.
        </p>
      </div>
      <Footer />
    </div>
  )
}