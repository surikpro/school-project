import React from 'react';
import { Sidebar } from '../Sidebar/Sidebar';
import { Navbar } from '../Navbar/Navbar';
import { Footer } from '../Footer/Footer';
import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from 'react-router-dom';
import fairyCharacter from "../../assets/images/fairy-character.jpeg";
const URL_GET_TOPIC = 'http://localhost:8080/api/v1/courses/english/topics';

export const Topic = () => {
    const { topicId } = useParams();
    let [loading, setLoading] = useState(true);
    let [topic, setTopic] = useState();
    const token = JSON.parse(localStorage.getItem('user'));
    useEffect(() => {
        console.log(topicId);
        const fetchData = async () => {
            setLoading(true);
                axios.get(URL_GET_TOPIC,
                    {
                        params: {
                            topicId: topicId
                        },
                        headers: {
                            'Authorization': `Bearer ${token.token}`
                        },
                    }
                )
                .then((response) => {
                    const topic = response.data;
                    setTopic(topic);
                })
            .catch ((error) => {
                // if (error.response.status === 404) {
                //     navigate("/error");
                // }
                console.log(error);
            })
            setLoading(false);
        }
        fetchData();
    }, []);
    if (!loading) {
        return (
            <div className='flex flex-col'>
                <Sidebar />
                <Navbar />
                <div className='flex flex-col border-4 border-gray-500 h-auto w-[70rem] mt-[8rem] text-3xl text-gray-700 rounded-md mx-auto'>
                    <h1 className='p-2 font-semibold text-gray-700 mx-auto'>{topic?.topicName}</h1>
                    <div className='flex p-2 text-2xl overflow-auto h-screen font-semibold relative'>
                        <p className='whitespace-pre-line p-2 text-justify w-[45rem] text-xl absolute'>
                            {topic?.description}
                        </p>
                        <img className='h-[30rem] rounded-lg absolute inset-y-6 right-3' src={fairyCharacter}></img>
                    </div>
                </div>
                <Footer />
            </div>
    
      )
    }
}