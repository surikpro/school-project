import React, { useState, useEffect, useRef } from "react";
import Confetti from "react-confetti-boom";
import axios from "axios";
import { Navbar } from "../Navbar/Navbar";
import { Footer } from "../Footer/Footer";
const URL_GET_TEST = 'http://localhost:8080/api/v1/test';
const URL_POST_TEST_RESULT = 'http://localhost:8080/api/v1/testResults';

export const Test = (props) => {
    let [loading, setLoading] = useState(true);
    let [data, setData] = useState();
    let [index, setIndex] = useState(0);
    let [lock, setLock] = useState(false);
    let [score, setScore] = useState(0);
    let [isTestCompleted, setTestCompleted] = useState(false);
    let token = JSON.parse(localStorage.getItem('user'))
    let Option1 = useRef(null);
    let Option2 = useRef(null);
    let Option3 = useRef(null);
    let Option4 = useRef(null);
    let option_array = [Option1, Option2, Option3, Option4];

    const checkAnswer = (e) => {
        if (lock === false) {
            if (data.questions[index].answer === e.target.innerText) {
                e.target.classList.add("bg-green-200");
                setLock(true);
                setScore(prevScore => prevScore + 1);
            } else {
                e.target.classList.add("bg-red-200");
                setLock(true);
                option_array[0].current.classList.add("bg-green-200");
            }
        }
    }

    const next = () => {
        if (lock === true) {
            setIndex(++index);
            setLock(false);
            option_array.map((option) => {
                option.current.classList.remove("bg-red-200");
                option.current.classList.remove("bg-green-200");
                return null;
                })
                if (data?.questions.length === index) {
                    setTestCompleted(true);
                    sendTestResult();
            }    
        }
    }

    const sendTestResult = () => {
        const test = {
        testId : data.testId
    }
        const user = {
        userId : data.user.userId
    }
        axios.post(URL_POST_TEST_RESULT, {
                score: score,
                test : test,
                user : user
            },
                {
                    headers : {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token.token}`
                }
            });
        
    }

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
                axios.get(URL_GET_TEST,
                    {
                        params: {
                            testName: props.testName
                        },
                        headers: {
                            'Authorization': `Bearer ${token.token}`
                        },
                    }
                )
                .then((response) => {
                    const test = response.data;
                    setData(test);
                })
            .catch ((error) => {
                // if (error.response.status === 404) {
                //     navigate("/error");
                // }
                console.log(error);
            })
            setLoading(false);
        }
        fetchData();
    }, []);

    if (isTestCompleted) {
            return (<div>
                <Navbar />
                <Confetti mode="boom" particleCount={200} shapeSize={20} colors={['#ff577f', '#ff884b', '#ffd384', '#fff9b0']}/>
                <div className="flex flex-col mx-auto mt-[140px] bg-gray-100 border-2 border-gray-500 rounded-md w-1/2 h-[30rem]">
                    <h1 className="mt-[5rem] text-gray-900 mx-auto text-4xl py-8 font-semibold">
                        Your test result is: {score}
                    </h1>
                    <hr class="h-1 rounded-md mx-auto my-8 bg-gray-500 border-0 dark:bg-gray-700 w-2/3"></hr>
                    <a href="/courses" className="mx-auto py-10 ">
                        <button className="mx-auto w-[340px] text-3xl hover:bg-gray-700 hover:text-white border-4 border-gray-700 bg-gray-400 rounded-md font-bold cursor-pointer px-10 py-5" onClick={next}>Наши курсы</button>
                    </a>
                </div>
            </div>);

    } else {
        if (!loading) {
            return (
                <div>
                    <Navbar />
                    <div className='flex flex-col w-2/5 mx-auto border-4 border-gray-700 bg-white text-gray-700 gap-6 mt-[120px] mb-6 rounded-md p-10'>
                        <h1 className="text-3xl font-bold text-center">Test your English level</h1>
                        <hr className="bg-gray-700 border-0 h-1 rounded-lg"/>
                            <h2 className="text-2xl text-left px-10 font-semibold">{index + 1}. {data?.questions[index].question}</h2>
                            <ul className="text-xl text-center border-4 border-gray-700 rounded-md font-semibold pl-[15] mb-4">
                                <li className="cursor-pointer border border-gray-700 text-left px-10 py-5 m-6 rounded-md" ref={Option1} onClick={(e) => {checkAnswer(e)}}>{data?.questions[index].questionChoices[0].questionChoice}</li>
                                <li className="cursor-pointer border border-gray-700 text-left px-10 py-5 m-6 rounded-md" ref={Option2} onClick={(e) => {checkAnswer(e)}}>{data?.questions[index].questionChoices[1].questionChoice}</li>
                                <li className="cursor-pointer border border-gray-700 text-left px-10 py-5 m-6 rounded-md" ref={Option3} onClick={(e) => {checkAnswer(e)}}>{data?.questions[index].questionChoices[2].questionChoice}</li>
                                <li className="cursor-pointer border border-gray-700 text-left px-10 py-5 m-6 rounded-md" ref={Option4} onClick={(e) => {checkAnswer(e)}}>{data?.questions[index].questionChoices[3].questionChoice}</li>
                            </ul>
                            <button className="mx-auto w-[340px] text-3xl hover:bg-gray-700 hover:text-white border-4 border-gray-700 bg-gray-400 rounded-md font-bold cursor-pointer px-10 py-5" onClick={next}>Next</button>
                            <div className="m-auto font-semibold text-xl">{index+1} of {data?.questions.length} questions</div>
                            <h2 className="text-center font-semibold text-xl">You scored {score} out of {data?.questions.length}</h2>
                    </div>
                    <Footer />
                </div>  
              )
        }
    }
}

