import React from "react";
import { Navbar } from "../Navbar/Navbar";
import { Footer } from "../Footer/Footer";
import { Banner } from "../Banner/Banner";
import { Sidebar } from "../Sidebar/Sidebar";

export const Home = () => {
    return (
        <div className="bg-gray-200">
            <Navbar />
            <Sidebar />
            <Banner />
            <Footer />
        </div>
    )
}
