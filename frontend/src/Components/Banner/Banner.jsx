import React from 'react';
import bannerLogo from "../../assets/images/front-banner.jpg";

export const Banner = () => {
  return (
    <div className="m-16 mb-10 bg-white shadow-xs border border-slate-200 rounded-lg w-2/3 mx-auto">
        <div className="h-2/3 m-1 overflow-hidden text-white rounded-md">
          <img src={bannerLogo} alt="banner" className="h-full w-full object-cover object-center rounded-md" />Баннер
        </div>
        <div className="p-4">
          <h6 className="mb-1 -mt-4 text-slate-800 text-6xl font-semibold">
          Вперед к знаниям!
          </h6>
        </div>
        <div className="px-4 pb-4 pt-0">
          <a href='/register'>
          <button className="rounded-md bg-orange-400 py-2 px-4 border border-transparent text-center text-4xl text-white transition-all shadow-md hover:shadow-lg focus:bg-slate-700 focus:shadow-none active:bg-slate-700 hover:bg-slate-700 active:shadow-none disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none" type="button">
          Начать обучение
          </button>
          </a>
        </div>
    </div>  
  )
}
