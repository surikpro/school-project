import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { jwtDecode } from "jwt-decode";
import { DropDownMenu } from "./DropDownMenu";
const URL_GET_COURSES = 'http://localhost:8080/api/v1/courses/findAll';
 
  export const EnglishCourseMenu = () => {
    let [topics, setTopics] = useState([]);
    let [courses, setCourses] = useState([]);
    let [loading, setLoading] = useState(true);
    const navigate = useNavigate();
    const token = JSON.parse(localStorage.getItem('user'));
    const jwtToken = null;
    const [isOpen, setIsOpen] = useState(false);
    const isTokenExpired = (jwtToken) => {
        jwtToken = token?.token;
        if (!jwtToken) return true;
        const decoded = jwtDecode(jwtToken);
        return decoded.exp*1000 + 60000*60 < Date.now();
      }

    useEffect(() => {
      const fetchData = async () => {
          setLoading(true);
              axios.get(URL_GET_COURSES,
                  {
                      headers: {
                          'Authorization': `Bearer ${token.token}`
                      },
                  }
              )
              .then((response) => {
                  const courses = response.data;
                  setCourses(courses);
                  setTopics(courses.topicsList);
                  console.log(courses);
              })
          .catch ((error) => {
              if (error.response === 404) {
                  navigate("/error");
              }
              console.log(error);
          })
          setLoading(false);
      }
      fetchData();
  }, []);
    
  if (!loading) {
    return (
      <div className="flex flex-col mx-auto w-3/4 space-y-2">
        {
        courses?.sort((a, b) => a.courseId - b.courseId).map((course, index) =>
          <DropDownMenu key={index} course={course}/>
        )}
      </div>
    );
  }
}