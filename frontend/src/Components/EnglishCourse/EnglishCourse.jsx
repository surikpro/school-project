import React from 'react';
import { Navbar } from "../Navbar/Navbar";
import { EnglishBanner } from './EnglishBanner';
import { EnglishCourseMenu } from './EnglishCourseMenu';
import { Sidebar } from '../Sidebar/Sidebar';
import { Footer } from '../Footer/Footer';

export const EnglishCourse = () => {
  return (
    <div className='bg-gray-50'>
        <Navbar />
        <Sidebar />
        <EnglishBanner />
        <EnglishCourseMenu />
        <Footer />
    </div>
  )
}