import { useState, } from "react";
import { IoIosAdd, IoIosRemove } from "react-icons/io";

export const DropDownMenu = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const topicsListLength = props.course?.topicsList.length;
    function determineLevelWriting(topicsListLength) {
        const getLastDigit = (num) => num %10;
        const lastDigit = getLastDigit(topicsListLength);
        switch (lastDigit) {
            case 1: return "уровень"; 
            case 2:
            case 3:
            case 4: return "уровня";
            case 0:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9: return "уровней";
        }
    }
  return (
        <div className='relative flex flex-col items-center w-2/3 mx-auto border-2 rounded-md bg-gray-100'>
          <button onClick={() => setIsOpen((prev) => !prev)} className='relative h-[80px] bg-gray-200 p-4 w-full flex items-center font-semibold text-lg tracking-wider rounded-t-md border-transparent active:border-gray-500 duration-300 active:text-gray-500'>
            {props.course?.courseName}
            <div className="text-left absolute right-[80px]">{topicsListLength} {determineLevelWriting(topicsListLength)}</div>
            {!isOpen ? (
              <IoIosAdd className="h-10 w-10 absolute right-3" />
            ) : (
              <IoIosRemove className="h-10 w-10 absolute right-3" />
            )}
          </button>
          {isOpen && (<div className='bg-gray-100 top-20 rounded-md w-full mt-2 mb-2'>
              {
                props.course?.topicsList.sort((a, b) => a.topicId - b.topicId).map((topic, index) => 
                <div key={index} className="flex font-semibold">
                  <div className='pl-4 w-32 text-gray-500 font-semibold'>Уровень {topic?.topicId}</div>
                  <div className='w-100 text-gray-700'>
                    <a href={"/courses/english/topics/" + topic?.topicId}>
                    {topic?.topicName}
                    </a>
                  </div>
                </div>
              )}
            </div>)}
    </div>
  )
}