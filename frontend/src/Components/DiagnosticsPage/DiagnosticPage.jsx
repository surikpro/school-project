import React from 'react';
import { Navbar } from "../Navbar/Navbar";
import { Test } from "../Test/Test"
import { useState } from 'react';
import { Footer } from '../Footer/Footer';
import { Sidebar } from '../Sidebar/Sidebar';
import { AiOutlineCaretDown, AiOutlineCaretUp } from 'react-icons/ai';

export const DiagnosticPage = () => {
    const [testName, setTestName] = useState("");
    const [isOpen, setIsOpen] = useState(false);
    const handleIsOpen = () => {
        setIsOpen((prev) => !prev);
    };
    const handleIsTestChosen = (value) => {
      setTestName(value);
      console.log("value is - " + typeof value);
      setTestChosen(true);
    }
    let [isTestChosen, setTestChosen] = useState(false);
  
    if (isTestChosen) {
      return (
        <div>
            <Navbar />
            <Sidebar />
            <Test testName={testName}/>; 
        </div>
      ) 
    } else {
      return (
        <div className='bg-gray-200'>
            <Navbar />
            <Sidebar />
            <div className="h-screen place-items-center justify-between hidden w-full md:flex md:w-auto md:order-1">
                <div className='relative flex flex-col items-center w-[340px] h-[340px] rounded-lg mx-auto'>
                  <button onClick={handleIsOpen} className='bg-white p-4 w-full flex items-center justify-between font-semibold text-lg rounded-md tracking-wider border-4 border-transparent active:border-gray-500 duration-300 active:text-orange-400'>
                    Выбери тест
                    {!isOpen ? (
                      <AiOutlineCaretDown className="h-8" />
                    ) : (
                      <AiOutlineCaretUp className="h-8" />
                    )}
                  </button>
                  {isOpen && (<div className='flex flex-col bg-white absolute top-20 rounded-md p-2 w-full'>
                    <div className='flex flex-col hover:p-2 w-full gap-3 rounded-lg font-semibold text-lg p-2'>
                      <h3 onClick={(e) => handleIsTestChosen("english_elementary")} className='hover:bg-gray-200 hover:border-gray-700 hover:border p-2 cursor-pointer rounded-md'>Английский базовый</h3>
                      <h3 className='hover:bg-gray-200 hover:border-gray-700 hover:border p-2 cursor-pointer rounded-md' value="english_intermediate">Английский средний</h3>
                      <h3 className='hover:bg-gray-200 hover:border-gray-700 hover:border p-2 cursor-pointer rounded-md' value="english_advanced">Английский продвинутый</h3>
                      <h3 className='hover:bg-gray-200 hover:border-gray-700 hover:border p-2 cursor-pointer rounded-md' value="math_basic">Математика базовый</h3>
                      <h3 className='hover:bg-gray-200 hover:border-gray-700 hover:border p-2 cursor-pointer rounded-md' value="math_hard">Математика усложненный</h3>
                    </div> 
                  </div>)}
                </div>
            </div>
          <Footer />
        </div>
      )
    }
}
