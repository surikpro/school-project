import React, { useState } from "react";
import axios from 'axios';
import registerLogo from "../../assets/registration-logo.svg";

export const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [birthday, setBirthday] = useState('');
    const handleSubmit = (e) => {
        e.preventDefault();
        axios.post("http://localhost:8080/api/v1/register", {
            firstName: firstName,
            lastName: lastName,
            birthday: birthday,
            email: email,
            password: password,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"},
        });
        
    }

    return (
        <section className="bg-gray-50 dark:bg-gray-900 font-bold">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto lg:py-0">
                <a href="/login" className="flex items-center mb-6 text-4xl text-gray-900 dark:text-white">
                    <img className="w-10 h-10 mr-2 mt-3" src={registerLogo} alt="logo"></img>
                    Регистрация
                </a>
                <div className="mx-auto w-full bg-white rounded-lg shadow-sm dark:border sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div>
                        <h1 className="ml-8 mt-3 text-xl leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                         Регистрация вашего аккаунта
                        </h1>
                        <form onSubmit={handleSubmit} className="mx-8 space-y-4 md:space-y-6" action="#">
                            <div>
                                <label htmlFor="email" className="block text-md text-gray-900 dark:text-white">
                                Ваше имя
                                </label>
                                <input value={firstName} onChange={(e) => setFirstName(e.target.value)} type="text" name="firstName" id="firstName"  placeholder="Ваше имя" required=""
                                className="bg-gray-100 border border-gray-400 border-solid text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"/>
                            </div>
                            <div>
                                <label htmlFor="lastName">Ваша фамилия</label>
                                <input value={lastName} onChange={(e) => setLastName(e.target.value)} type="text" placeholder="Ваша фамилия" id="lastName" name="lastName" 
                                className="bg-gray-100 border border-gray-400 border-solid text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"/>
                            </div>
                            <div>
                                <label htmlFor="birthday">Ваш день рождения</label>
                                <input value={birthday} onChange={(e) => setBirthday(e.target.value)} type="date" placeholder="Ваш день рождения" id="birthday" name="birthday" 
                                className="bg-gray-100 border border-gray-400 border-solid text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"/>
                            </div>
                            <div>
                                <label htmlFor="email">Ваша почта</label>
                                <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="youremail@gmail.com" id="email" name="email" 
                            className="bg-gray-100 border border-gray-400 border-solid text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"/>
                            </div>
                            <div>
                                <label htmlFor="password">Ваш пароль</label>
                                <input value={password} onChange={(e) => setPassword(e.target.value)} type="password" placeholder="*******" id="password" name="password" 
                                className="bg-gray-100 border border-gray-400 border-solid text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"/>
                            </div>
                            <button type="submit" className="my-2 w-full text-2xl bg-gray-500 text-gray-900 hover:bg-primary-700 focus:ring-4 focus:outline-hidden focus:ring-primary-300 rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                                Регистрация
                            </button>
                            <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                                Уже есть аккаунт? <a href="/login" className="font-bold text-primary-600 hover:underline dark:text-primary-500">Войти</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    )
}