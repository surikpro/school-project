import coursesBanner from "../../assets/coursesBanner.png";
   
  export function CourseBanner() {
    return (
      <div className="flex flex-row mt-[120px] w-3/4 mx-auto border rounded-md">
        <div className="flex flex-col gap-4 px-8 py-2">
          <h4 className="mt-10 blue-gray-900 text-4xl font-bold">
            Курсы от KIDS ACADEMY
          </h4>
          <p className="mb-8 mt-6 font-semibold text-xl">
          Удивительные интерактивные курсы от Kids Academy помогли миллионам людей сделать первые шаги в освоении и 
          изучения английского языка, олимпиадной математики и шахматных дисциплин. Главная особенность наших курсов — 
          простата теории, практика, персональный подход и интерактивность. Учись в своем темпе и получай удовольствие от обучения.
          </p>
          <a href="/register" className="inline-block">
            <button className="flex items-center gap-2 font-bold text-3xl border rounded-lg p-2 bg-gray-300 hover:text-white hover:bg-gray-700">
              Начать обучение
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
                className="h-8 w-8 mt-2"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
                />
              </svg>
            </button>
          </a>
        </div>
        <div className="flex flex-col h-auto max-w-full m-2 shrink-0 rounded-lg border">
          <img
            src={coursesBanner}
            alt="courses-banner-image"
            className="h-100 w-60 rounded-lg"
          />
        </div>
      </div>
    );
  }