import React from 'react';
import { Navbar } from "../Navbar/Navbar";
import { CourseCard } from './CourseCard';
import { CourseBanner } from './CourseBanner';
import { Footer } from '../Footer/Footer';
import { Sidebar } from '../Sidebar/Sidebar';

export function CoursesPage() {
  return (
    <div className='flex flex-col bg-gray-100'>
      <Sidebar />
      <Navbar />
      <CourseBanner />
      <CourseCard />
      <Footer />
    </div>
  )
}
