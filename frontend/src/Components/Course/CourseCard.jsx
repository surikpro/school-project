import englishLogo from '../../assets/english-logo.jpeg';
import mathLogo from '../../assets/math.jpeg';
import chessLogo from '../../assets/chess.jpeg';

  export function CourseCard() {
    return (
        <div className="flex justify-center ml-[104px]">
            <div className="flex flex-col m-5 w-96 h-[30] border bg-gray-200 rounded-md"> 
              <div className="relative h-56 w-96 blue-gray">
                <img
                  src={englishLogo}
                  alt="english-image"
                  className='rounded-t-md h-64 w-96'/>
              </div>
              <div className='flex flex-col relative mt-10'>
                <h5 className="text-center blue-gray text-2xl font-semibold">
                  Курс Английский язык
                </h5>
                <p className='p-2 m-2'>
                  Легендарный курс по английскому языку доступен сейчас онлайн, 
                  который помог тысячам начать говорить на английском языке. Освойте английский язык с нуля: начиная от азов 
                  до уровня носителя языка.
                </p>
              </div>
              <div className="text-center mx-auto w-1/3 h-16 mt-auto">
                <a href="/courses/english">
                  <button className='border-2 border-gray-700 rounded-md bg-gray-300 p-2 text-xl font-bold cursor:pointer hover:text-white hover:bg-gray-700'>Купить</button>
                </a>
              </div>
            </div>
            <div className="flex flex-col m-5 w-96 h-[30] border bg-gray-200 rounded-md"> 
              <div className="relative h-56 w-96 blue-gray">
                <img
                  src={mathLogo}
                  alt="card-image"
                  className='rounded-t-md h-64 w-96'/>
              </div>
              <div className='flex flex-col relative mt-10'>
                <h5 className="text-center blue-gray text-2xl font-semibold">
                Курс Занимательная математика
                </h5>
                <p className='p-2 m-2'>
                  Легендарный курс по занимательной математике доступен сейчас онлайн, 
                  который помог тысячам начать увлекательное путешествие в мир чисел. Освойте математику с нуля: 
                  начиная от азов до уровня профи.
                </p>
              </div>
              <div className="text-center mx-auto w-1/3 h-16 mt-auto">
              <a href="/courses/math">
                  <button className='border-2 border-gray-700 rounded-md bg-gray-300 p-2 text-xl font-bold cursor:pointer hover:text-white hover:bg-gray-700'>Купить</button>
                </a>
              </div>
            </div>
            <div className="flex flex-col m-5 w-96 h-[30] border bg-gray-200 rounded-md"> 
              <div className="relative h-56 w-96 blue-gray">
                <img
                  src={chessLogo}
                  alt="card-image"
                  className='rounded-t-md h-64 w-96'/>
              </div>
              <div className='flex flex-col relative mt-10'>
                <h5 className="text-center blue-gray text-2xl font-semibold">
                Курс Увлекательные шахматы
                </h5>
                <p className='p-2 m-2'>
                  Легендарный курс по увлекательным шахматам доступен сейчас онлайн, 
                  который помог тысячам начать увлекательное путешествие в мир шахматных фигур. Освойте шахматы с нуля: 
                  начиная от азов до уровня профи.
                </p>
              </div>
              <div className="text-center mx-auto w-1/3 h-16 mt-auto">
              <a href="/courses/math">
                  <button className='border-2 border-gray-700 rounded-md bg-gray-300 p-2 text-xl font-bold cursor:pointer hover:text-white hover:bg-gray-700'>Купить</button>
                </a>
              </div>
            </div>
        </div>
    );
  }