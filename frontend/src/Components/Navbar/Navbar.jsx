import { jwtDecode } from "jwt-decode";

export function Navbar() {

  const token = JSON.parse(localStorage.getItem('user'));
  const jwtToken = null;
  const isTokenExpired = (jwtToken) => {
    jwtToken = token?.token;
    if (!jwtToken) return true;
    const decoded = jwtDecode(jwtToken);
    return decoded.exp*1000 + 60000 < Date.now();
  }
  console.log("isExpired - " + isTokenExpired(jwtToken));
  
  const logout = () => {
    localStorage.removeItem('user');
  }

  const navList = (
    <ul className="flex flex-col gap-2 lg:mb-0 lg:mt-0 lg:flex-row lg:items-center lg:gap-3 p-1">
      <li className="p-2 font-bold text-lg blue-gray hover:text-white">
        <a href="/about">
          О нас
        </a>
      </li>
      <li className="p-2 font-bold text-lg blue-gray hover:text-white">
        <a href="/diagnostics">
          Диагностика
        </a>
      </li>
      <li className="p-2 font-bold text-lg blue-gray hover:text-white">
        <a href="/courses">
          Курсы
        </a>
      </li>
      <li className="p-2 font-bold text-lg blue-gray hover:text-white">
        <a href="/profile">
        Личный кабинет
        </a>
      </li>
    </ul>
  );
 
  return (
    <div>
      <nav className="fixed top-0 z-10 h-16 w-full rounded-none border bg-gray-500">
        <div className="flex justify-between text-xl text-blue-gray-900">
          <a href="/" className="hover:text-white hover:bg-gray-700 hover:p-2 hover:border-1 hover:rounded flex items-center space-x-3 rtl:space-x-reverse">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-10 h-10">
              <path d="M11.7 2.805a.75.75 0 0 1 .6 0A60.65 60.65 0 0 1 22.83 8.72a.75.75 0 0 1-.231 1.337 49.948 49.948 0 0 0-9.902 3.912l-.003.002c-.114.06-.227.119-.34.18a.75.75 0 0 1-.707 0A50.88 50.88 0 0 0 7.5 12.173v-.224c0-.131.067-.248.172-.311a54.615 54.615 0 0 1 4.653-2.52.75.75 0 0 0-.65-1.352 56.123 56.123 0 0 0-4.78 2.589 1.858 1.858 0 0 0-.859 1.228 49.803 49.803 0 0 0-4.634-1.527.75.75 0 0 1-.231-1.337A60.653 60.653 0 0 1 11.7 2.805Z" />
              <path d="M13.06 15.473a48.45 48.45 0 0 1 7.666-3.282c.134 1.414.22 2.843.255 4.284a.75.75 0 0 1-.46.711 47.87 47.87 0 0 0-8.105 4.342.75.75 0 0 1-.832 0 47.87 47.87 0 0 0-8.104-4.342.75.75 0 0 1-.461-.71c.035-1.442.121-2.87.255-4.286.921.304 1.83.634 2.726.99v1.27a1.5 1.5 0 0 0-.14 2.508c-.09.38-.222.753-.397 1.11.452.213.901.434 1.346.66a6.727 6.727 0 0 0 .551-1.607 1.5 1.5 0 0 0 .14-2.67v-.645a48.549 48.549 0 0 1 3.44 1.667 2.25 2.25 0 0 0 2.12 0Z" />
              <path d="M4.462 19.462c.42-.419.753-.89 1-1.395.453.214.902.435 1.347.662a6.742 6.742 0 0 1-1.286 1.794.75.75 0 0 1-1.06-1.06Z" />
            </svg>
          <span className="self-center text-xl font-bold whitespace-nowrap dark:text-white cursor-pointer">KIDS ACADEMY</span>
          </a>  
          <div className="flex items-center gap-4 p-1">
            <div className="mr-4 mb-0 hidden lg:block">{navList}</div>
            <div className="flex items-center gap-x-1">

            {!isTokenExpired(jwtToken) ?
            <a href="/">
            <button onClick={logout} className="hidden border-1 bg-gray-100 rounded-md lg:inline-block mx-2 text-lg font-bold hover:text-gray-500 p-1">
              <span>Выйти</span>
            </button>
            </a> 
              :
              <a href="/login">
              <button className="hidden border-1 bg-gray-100 rounded-md lg:inline-block mx-2 text-lg  font-bold hover:text-gray-500 p-1">
                <span>Войти</span>
              </button>
              </a>
              }
              <a href="/register">
              <button className="hidden border-1 bg-gray-100 rounded-md lg:inline-block mr-4 f text-lg font-bold hover:text-gray-500 p-1"
              >
                <span>Регистрация</span>
              </button>
              </a>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}
