import React, { useState } from "react";
import { Navigate } from "react-router-dom";
import axios from "axios";
import loginLogo from "../../assets/login-logo.svg";

const AUTH_API_URL = "http://localhost:8080/api/v1/auth/authenticate";

export const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    let [isLoggedIn, setLoggedIn] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const response = await axios
            .post(AUTH_API_URL, {
                email,
                password,
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json"},
            },
        );
        if (response.data) {
            console.log(response.data)
            localStorage.setItem("user", JSON.stringify(response.data));
            setLoggedIn(true);
        }
        return response.data;
    
    }
    if (isLoggedIn) {
        return <Navigate to="/" />;
    } else {
        return (
            <section className="bg-gray-50 dark:bg-gray-900 font-bold">
                <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <a href="/login" className="flex items-center mb-6 text-4xl text-gray-900 dark:text-white">
                    <img className="w-8 h-8 mr-2" src={loginLogo} alt="logo">
                    </img>
                    Login
                </a>
                <div className="w-full bg-white rounded-lg shadow-sm dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div>
                        <h1 className="ml-8 mt-6 text-xl leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                        Войти в аккаунт
                    </h1>
                    <form onSubmit={handleSubmit} className="mx-8 my-5 space-y-4 md:space-y-6" action="#">
                        <div>
                            <label htmlFor="email" className="block text-md text-gray-900 dark:text-white">
                            Ваша почта
                            </label>
                            <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" name="email" id="email" className="bg-gray-100 border border-gray-400 border-solid text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" required="">
                            </input>
                        </div>
                        <div>
                            <label htmlFor="password" className="block text-md text-gray-900 dark:text-white">Ваш пароль</label>
                            <input value={password} onChange={(e) => setPassword(e.target.value)} type="password" name="password" id="password" placeholder="••••••••" className="bg-gray-100 border border-gray-400 border-solid text-gray-900 rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
                            </input>
                        </div>
                        <div className="text-right"> 
                            <a href="#" className="text-sm text-primary-600 hover:underline dark:text-primary-500">Забыли пароль?</a>
                        </div>
                        <button type="submit" className="my-2 w-full text-2xl text-gray-900 bg-gray-500 hover:bg-primary-700 focus:ring-4 focus:outline-hidden focus:ring-primary-300 rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                            Войти
                        </button>
                        <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                            Нет еще аккаунта? <a href="/register" className="font-bold text-primary-600 hover:underline dark:text-primary-500">Зарегистрироваться</a>
                        </p>
                    </form>
                    </div>
                </div>
                </div>
            </section>
        )
    }
}