import React, { useState} from "react";
import { Login } from "./Components/Login/Login";
import { Register } from "./Components/Register/Register";
import { Home } from "./Components/Home/Home";
import { Test } from "./Components/Test/Test";
import { ErrorPage } from "./Components/ErrorPage/ErrorPage";
import { About } from "./Components/About/About";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { DiagnosticPage } from "./Components/DiagnosticsPage/DiagnosticPage";
import { CoursesPage } from "./Components/Course/CoursesPage";
import { EnglishCourse } from "./Components/EnglishCourse/EnglishCourse";
import { Topic } from "./Components/Topic/Topic";
import { Topics } from "./Components/Topic/Topics";

function App() {
  const [currentForm, setCurrentForm] = useState('login');

  const toggleForm = (formName) => {
    setCurrentForm(formName);
  }
  return (
    <Router>
      <Routes>
        <Route path="/login" element={<div className="App">
      {
           currentForm === "login" ? <Login onFormSwitch={toggleForm} /> : <Register onFormSwitch={toggleForm} />
      }
    </div>} />
        <Route path="/register" element={<Register/>} />
        <Route path="/" element={<Home />} />
        <Route path="/test" element={<Test />} />
        <Route path="/about" element={<About />} />
        <Route path="/diagnostics" element={<DiagnosticPage />} />
        <Route path="/courses" element={<CoursesPage />} />
        <Route path="/courses/english" element={<EnglishCourse />} />
        <Route path="/courses/english/topics/:topicId" element={<Topic />} />
        <Route path="/courses/{courseName}/tasks" element={<Topics />} />
        <Route path="/*" element={<ErrorPage />} />
      </Routes>
    </Router>
    
  );
}

export default App;
